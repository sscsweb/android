/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejb;

import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Valeria
 */
@Local
public interface UtenteFacadeLocal {

    void create(Utente utente);

    void edit(Utente utente);

    void remove(Utente utente);

    Utente findByLogin(String urs, String psw);

    List<Utente> findAll();

    List<Utente> findRange(int[] range);

    int count();

    Utente findByMail(String username);
    
}
