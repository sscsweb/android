/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejb;

import java.util.Date;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.faces.view.Location;

/**
 *
 * @author Valeria
 */
@Stateless
public class GestoreAppuntamento implements GestoreAppuntamentoLocal {
    @EJB
    private MenuFacadeLocal menuFacade;
    @EJB
    private AppuntamentoFacadeLocal appuntamentoFacade;

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
    public void aggiungiAppuntamento(Utente cuoco, String tipo, String[] portate, String[] ingredienti, int capienza, Date data, Location luogo){
        Menu menu = new Menu();
        menu.setTipo(tipo);
        menu.setPortate(portate);
        menu.setIngredienti(ingredienti);
        menuFacade.create(menu);
        
        Appuntamento a = new Appuntamento();
        a.setCuoco(cuoco);
        a.setMenu(menu);
        a.setCapienza(capienza);
        a.setGiorno(data);
        a.setLuogo(luogo);
        appuntamentoFacade.create(a);
    }
}
