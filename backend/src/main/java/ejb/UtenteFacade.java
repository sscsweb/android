/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejb;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Valeria
 */
@Stateless
public class UtenteFacade extends AbstractFacade<Utente> implements UtenteFacadeLocal {
    @PersistenceContext(unitName = "SSCSWEB_CS-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UtenteFacade() {
        super(Utente.class);
    }

    @Override
    public List<Utente> findAll() {
        return super.findAll(); //To change body of generated methods, choose Tools | Templates.
    }

    /*public boolean findUser(String mail) {
        List<Utente> results = em.createQuery("SELECT u.nome FROM Utente u WHERE u.mail ='"+mail+"'", Utente.class)
                        .getResultList();
        return results.isEmpty();
    }*/

    @Override
    public void remove(Utente entity) {
        super.remove(entity); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void create(Utente entity) {
        super.create(entity); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Utente findByMail(String mail) {
        Utente result;
        try {
            result = (Utente) em.createQuery("SELECT u FROM Utente u WHERE u.mail ='"+mail+"'")
                            .getSingleResult();
        } catch (Exception e){
            result = null;
        }
        return result;
    }
    
    /**
     *
     * @param usr
     * @param psw
     * @return
     */
    @Override
    public Utente findByLogin (String usr, String psw){
        Utente result;
        try {
            result = (Utente) em.createQuery("SELECT u FROM Utente u WHERE u.username ='"+usr+"' AND u.password='"+psw+"'")
                        .getSingleResult();
        } catch (Exception e){
            System.out.println("Couldn't find any record corresponding to user" +usr + " with pass "+psw);
            result = null;
        }
        return result;
    }
    
}
