/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejb;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Valeria
 */
@Stateless
public class AppuntamentoFacade extends AbstractFacade<Appuntamento> implements AppuntamentoFacadeLocal {
    @PersistenceContext(unitName = "SSCSWEB_CS-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public AppuntamentoFacade() {
        super(Appuntamento.class);
    }
    
}
