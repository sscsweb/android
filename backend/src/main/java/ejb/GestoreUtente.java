/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejb;

import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Valeria
 */
@Stateless
public class GestoreUtente implements GestoreUtenteLocal {
    @EJB
    private UtenteFacadeLocal utenteFacade;

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
    @Override
     public void aggiungiUtente(String usr, String psw, String nome, String cognome, String mail, String ind, String citta){
        Utente u = new Utente();
        u.setUsername(usr);
        u.setPassword(psw);
        u.setNome(nome);
        u.setCognome(cognome);
        u.setMail(mail);
        u.setIndirizzo(ind);
        u.setCitta(citta);
        utenteFacade.create(u);
    }
     
    /**
     *
     * @param mail
     * @return
     */
    @Override
    
    public Utente getUserByMail(String mail){
        return utenteFacade.findByMail(mail);
    }
    
    /**
     *
     * @param usr
     * @param psw
     * @return
     */
    @Override
    public Utente getUserByLogin(String usr, String psw){
        return utenteFacade.findByLogin(usr, psw);
    }
}
