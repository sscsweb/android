/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejb;

import javax.ejb.Local;

/**
 *
 * @author Valeria
 */
@Local
public interface GestoreUtenteLocal {
    public void aggiungiUtente(String usr, String psw, String nome, String cognome, String mail, String ind, String citta);
    public Utente getUserByMail(String mail);
    public Utente getUserByLogin(String usr, String psw);
}
