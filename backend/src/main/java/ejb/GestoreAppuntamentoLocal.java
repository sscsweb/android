/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejb;

import java.util.Date;
import javax.ejb.Local;
import javax.faces.view.Location;

/**
 *
 * @author Valeria
 */
@Local
public interface GestoreAppuntamentoLocal {
    public void aggiungiAppuntamento(Utente cuoco, String tipo, String[] portate, String[] ingredienti, int capienza, Date data, Location luogo);
}
