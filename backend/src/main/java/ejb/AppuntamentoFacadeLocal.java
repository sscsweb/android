/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejb;

import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Valeria
 */
@Local
public interface AppuntamentoFacadeLocal {

    void create(Appuntamento appuntamento);

    void edit(Appuntamento appuntamento);

    void remove(Appuntamento appuntamento);

    Appuntamento find(Object id);

    List<Appuntamento> findAll();

    List<Appuntamento> findRange(int[] range);

    int count();
    
}
