/*
   For step-by-step instructions on connecting your Android application to this backend module,
   see "App Engine Java Servlet Module" template documentation at
   https://github.com/GoogleCloudPlatform/gradle-appengine-templates/tree/master/HelloWorld
*/

package com.example.Nicola.myapplication.backend;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
FB id Vale: 671481826304571
 */

        import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
        import java.io.PrintWriter;
        import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ejb.GestoreAppuntamentoLocal;
import ejb.GestoreUtenteLocal;
import ejb.Utente;

/**
 *
 * @author Valeria
 */
public class ServletController extends HttpServlet {

    @EJB
    private GestoreAppuntamentoLocal gestoreAppuntamento;
    @EJB
    private GestoreUtenteLocal gestoreUtente;
    String u = "";
    String p = "";
    String n = "";
    String c = "";
    String m = "";
    String i = "";
    String city = "";
    HttpSession session;
    RequestDispatcher rd;
    String action;
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     * @throws org.json.JSONException
     * @throws java.lang.ClassNotFoundException
     * @throws java.sql.SQLException
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, JSONException, ClassNotFoundException, SQLException {
        session = request.getSession();
        try{
            String q = session.getAttribute("username").toString();
            System.out.println("Logged in as "+ q);
        } catch(Exception k){System.out.println("No user logged in.");}
        response.setContentType("text/html;charset=UTF-8");

        rd = getServletContext().getRequestDispatcher("/fail");

        response.setContentType("text/html;charset=UTF-8");
        action = request.getParameter("action");

        try {
            PrintWriter out = response.getWriter();
            /* TODO output your page here. You may use following sample code. */
            switch(action){
                case "addUtenteNew":
                    System.out.println("Aggiunta nuovo utente");
                    m = request.getParameter("user[email]");
                    if (gestoreUtente.getUserByMail(m) == null){     //utente non registrato
                        u = request.getParameter("user[username]");
                        p = request.getParameter("user[password]");
                        n = request.getParameter("user[first_name]");
                        c = request.getParameter("user[last_name]");
                        i = request.getParameter("user[address]");
                        city = request.getParameter("user[city]");
                        gestoreUtente.aggiungiUtente(u, p, n, c, m, i, city);
                    } else {
                        //TODO POPUP "SEI GI� REGISTRATO!"
                        System.out.println("Sei gi� registrato");
                        action = "login";
                    }
                    break;

                case "FBlogin":
                    System.out.println("Aggiunta utente FB");
                    try{
                        String code = request.getParameter("code");
                        if (code == null || code.equals("")) {
                            System.out.println("Error while getting request code");
                            action = "error";
                            // an error occurred, handle this
                        }
                        else{
                            System.out.println("Request code acquired");
                            String token;

                            String g;
                            g = "https://graph.facebook.com/oauth/access_token?client_id=603586926442128&redirect_uri=" + URLEncoder.encode("http://localhost:8080/SSCSWEB_CS-war/ServletController?action=FBlogin", "UTF-8") + "&client_secret=6946aa760c06a48de34c9a5fe0dd5c54&code=" + code;
                            URL url = new URL(g);
                            URLConnection con = url.openConnection();
                            StringBuffer b;
                            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
                            String inputLine;
                            b = new StringBuffer();
                            while ((inputLine = in.readLine()) != null){
                                b.append(inputLine).append("\n");
                            }
                            token = b.toString();
                            if (token.startsWith("{")){
                                throw new Exception("error on requesting token: " + token + " with code: " + code);
                            }

                            String graph;

                            g = "https://graph.facebook.com/me?" + token;
                            url = new URL(g);
                            con = url.openConnection();
                            in = new BufferedReader(new InputStreamReader(con.getInputStream()));
                            b = new StringBuffer();
                            while ((inputLine = in.readLine()) != null){
                                b.append(inputLine).append("\n");
                            }
                            graph = b.toString();


                            //String facebookId; servir� per accesso
                            JSONObject json = new JSONObject(graph);

                            if (gestoreUtente.getUserByMail(json.getString("email")) == null){
                                System.out.println("Adding User to the DB");
                                //facebookId = json.getString("id");
                                n = json.getString("first_name");
                                c = json.getString("last_name");
                                m = json.getString("email");
                                p = GeneraPSW.generateRandomPassword();
                                gestoreUtente.aggiungiUtente(m, p, n, c, m, i, city);
                                System.out.println("Aggiungo "+n+" al DB con questa email: "+m );
                            } else {
                                //TODO POPUP "SEI GI� REGISTRATO!"
                                m = json.getString("email");
                                u = gestoreUtente.getUserByMail(m).getUsername();
                                System.out.println("User "+ u +" logged in successfully");

                            }

                            break;
                        }
                    }
                    catch(Exception k){System.out.println(k);}
                case "addAppuntamento": {

                }

                case "login":{
                    System.out.println("Login");
                    m = (m.equals(""))? request.getParameter("user[email]"):m;
                    p = (p.equals(""))? request.getParameter("user[password]"):p;
                    try{
                        u = gestoreUtente.getUserByMail(m).getUsername();
                    } catch(Exception p){System.out.println("Exception "+p+" raised while trying to fetch datas for mail "+m + " with pass "+p);}
                    System.out.println("user: "+u+"; mail: "+m+"; pass: "+p);
                    try{
                        gestoreUtente.getUserByLogin(u,p);
                    } catch (Exception e){
                        u ="non ";
                    }
                    System.out.println("Utente "+u+" registrato con la mail: "+m);
                    if(m == null){ action = "error";}//utente non registrato
                }

            }

            if ("error".equals(action)){
                System.out.println("Setting action value to \'error\' and going back ");
                request.setAttribute("action", action);
                rd = getServletContext().getRequestDispatcher("/redir.jsp");
                //PRESI DAL DB!
            } else {
                JSONObject json = new JSONObject();
                Utente utente = gestoreUtente.getUserByMail(m);
                json.put("username", utente.getUsername());
                json.put("password", utente.getPassword());
                json.put("nome", utente.getNome());
                json.put("cognome", utente.getCognome());
                json.put("mail", utente.getMail());
                json.put("indirizzo", utente.getIndirizzo());
                json.put("citta", utente.getPassword());
                request.setAttribute("action", action);
                //dopo la registrazione o il login, rimanda alla pagina del profilo
                session.setAttribute("username", u);
                switch (action) {
                    case "error":
                        rd = getServletContext().getRequestDispatcher("/login.jsp");
                        break;
                    case "addUtenteNew":
                        rd = getServletContext().getRequestDispatcher("/redir.jsp");
                        break;
                    case "login":
                        rd = getServletContext().getRequestDispatcher("/redir.jsp");
                        break;
                    case "FBlogin":
                        rd = getServletContext().getRequestDispatcher("/redir.jsp");
                        break;
                }
                request.setAttribute("jsonObject", json);
            }
            rd.forward(request, response);
        }
        catch(Exception k){
            System.out.println("Eccezione: "+k);
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        try {
            processRequest(request, response);
        } catch (JSONException | ClassNotFoundException | SQLException ex) {
            Logger.getLogger(ServletController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (JSONException | ClassNotFoundException | SQLException ex) {
            Logger.getLogger(ServletController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}